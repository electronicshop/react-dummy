react-dummy provides a component which can be used as outer tag for a component which has to render two or more top level sub components.
It does not render anything except it's childs html. 

```
<Dummy>
    <div>A</div>
    <div>B</div>
    <div>C</div>
</Dummy>
```