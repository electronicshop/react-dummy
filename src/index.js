import React from "react";

class Dummy extends React.Component {
    render() {
        return this.props.children;
    }
}

module.exports = {
    Dummy
}
